  /* Функции

1. Создать функцию `multiply`, которая будет принимать любое количество чисел и возвращать их 
произведение: `multiply(1,2,3) = 6 (1*2*3)`

 Если нет ни одного аргумента, вернуть ноль: `multiply() // 0`*/
let array = []
function multiply(array) {
    let productNumbers = 1;
    for (i = 0; i < array.length; i++) {
        productNumbers *= array[i];
    }
    console.log(productNumbers);
}

multiply([2,3,4]);
 

/*2. Создать функцию, которая принимает строку и возвращает строку-перевертыш: `reverseString(‘test’) // “tset”`.*/

function reverseString(strArg) {
    let revertStr = '';
    for (let i = strArg.length - 1; i >= 0; i--) {
        revertStr += strArg.charAt(i)
    }
    console.log(revertStr);
}

reverseString('test');


/*3. Создать функцию, которая в качестве аргумента принимает строку из букв и возвращает строку, где каждый символ
 разделен пробелом и заменен на юникод-значение символа: 

`getCodeStringFromText(‘hello’) // “104 101 108 108 111” `

  * подсказка: для получения кода используйте специальный метод */

function getCodeStringFromText(strArgHi) {
    let newUnicodeStr = '';
    for (let i = 0; i < strArgHi.length; i++) {
       newUnicodeStr += strArgHi.charCodeAt(i) + " ";
    }
    console.log(newUnicodeStr.trim());
}

getCodeStringFromText('hello');


/*4. Создать функцию угадай число. Она принимает число от 1-10 (обязательно проверить что число не больше 10 и не меньше 0).
 Генерирует рандомное число от 1-10 и сравнивает с переданным числом если они совпали то возвращает “Вы выиграли” если нет
 то “Вы не угадали ваше число 8 а выпало число 5”. Числа в строке указаны как пример вы подставляете реальные числа.*/


function guessTheNumber(myNumber) {
    let number = Math.floor((Math.random() * 10) + 1);
    // console.log('загаданное число ', number)

    if (myNumber > 0 && myNumber <= 10 && myNumber === number) {
        console.log('Вы выиграли');

    } else if (myNumber < 0 || myNumber > 10) {
        console.log('число дожно быть в пределе от 1 до 10');
    } else {
        console.log(`Вы не угадали ваше число ${myNumber} а выпало число ${number}`);
    }
}

guessTheNumber(8);


/*5. Создать функцию, которая принимает число n и возвращает массив, заполненный числами 
от 1 до n: `getArray(10); // [1,2,3,4,5,6,7,8,9,10]`*/

function getArray(n) {
    let arr = [];
    for (let i = 0; i < n; i++){
        arr[i] = i + 1;
    }
    console.log(arr);
}

getArray(19);


  /*6. Создать функцию, которая принимает массив, а возвращает новый массив с дублированными элементами входного массива:
`doubleArray([1,2,3]) // [1,2,3,1,2,3]`*/

function doubleArray(array) {
    let doubleArray = [];
    doubleArray = array;
    console.log(array.concat(doubleArray));
}

doubleArray([2,5,8,0]);


/*7. Создать функцию, которая принимает произвольное (любое) число массивов и удаляет из каждого массива первый элемент,
   а возвращает массив из оставшихся значений: 
`changeCollection([1,2,3], [‘a’, ’b’, ‘c’]) → [ [2,3], [‘b’, ‘c’] ], changeCollection([1,2,3]) → [ [2,3] ] и т.д.`*/


// function changeCollection(array) {

//     let handler = arguments[arguments.length - 1];

//     for (let i = 0; i < arguments.length - 1; i++) {
//         let array = arguments[i];
//         handler(array);
//     }
    
// }
// function deleteFunc(array) {
//   array.shift();
// }

// console.log(changeCollection([1,2,3], ['a', 'b', 'c'], deleteFunc));





  /*8. Создать функцию которая принимает массив пользователей, поле на которое хочу проверить и значение на которое 
  хочу проверять. Проверять что все аргументы переданы. Возвращать новый массив с пользователями соответсвующие указанным
   параметрам.

`funcGetUsers(users, “gender”, “male”); // [ {name: “Denis”, age: “29”, gender: “male”} ,
 {name: “Ivan”, age: “20”, gender: “male”} ]`
*/


// let users = [
//     {name: 'Denis',  age: '29',  gender: 'male'},
//     {name: 'Ivan',   age: '20',  gender: 'male'},
//     {name: 'Name',   age: '28',  gender: 'female'}
// ]
// function funcGetUsers(usersArr, key, value){
//    let result = usersArr.filter(elem => elem == key);
//    console.log(result);
// }
// funcGetUsers(users, 'gender', 'male');




// Функции высшего порядка. Задачи.

  /*1. Создать две функции и дать им осмысленные названия:
- первая функция принимает массив и колбэк (одна для всех вызовов)
- вторая функция (колбэк) обрабатывает каждый элемент массива (для каждого вызова свой callback)

Первая функция возвращает строку `“New value: ”` и результат обработки:

```javascript
firstFunc([‘my’, ‘name’, ‘is’, ‘Trinity’], handler1) → “New value: MyNameIsTrinity”
firstFunc([10, 20, 30], handler2) → “New value: 100, 200, 300,”
firstFunc([{age: 45, name: ‘Jhon’}, {age: 20, name: ‘Aaron’}], handler3) →
“New value: Jhon is 45, Aaron is 20,”
firstFunc([‘abc’, ‘123’], handler4) → “New value: cba, 321,” // строки инвертируются
```

Подсказка: `secondFunc` должна быть представлена функцией, которая принимает
один аргумент (каждый элемент массива) и возвращает результат его обработки*/


function handler(arr, handler) {

    return handler(revertArr)
}

function revertArr() {

}





  /*2. Написать аналог метода every. Создайте функцию every, она должна принимать первым аргументом массив чисел 
  (обязательно проверьте что передан массив) вторым аргументом `callback` (обязательно проверьте что передана функция)
функция должна возвращать `true` или `false` в зависимости от результата вызова `callback` (проверить число больше 5).
 `Callback` должен принимать один элемент массива, его индекс в массиве и весь массив. 
 
Что такое метод `every` можно прочитать [здесь](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/every).
*/








// Массивы. Перебирающие методы. Задачи.

  /*1. На основе массива `[1,2,3,5,8,9,10]` сформировать новый массив,
каждый элемент которого будет хранить информацию о числе и его четности:
```javascript
[{digit: 1, odd: true}, {digit: 2, odd: false}, {digit: 3, odd: true}...]
```*/







  /*2. Проверить, содержит ли массив `[12, 4, 50, 1, 0, 18, 40]` элементы, равные нулю. Если да - вернуть `false`.*/









  /*3. Проверить, содержит ли массив `['yes', 'hello', 'no', 'easycode', 'what']` хотя бы одно слово длиной 
  больше 3х букв. Если да - вернуть `true`
Дан массив объектов, где каждый объект содержит информацию о букве и месте её положения в строке `{буква: “a”,
 позиция_в_предложении: 1}`:

```javascript
[{char:"a",index:12}, {char:"w",index:8}, {char:"Y",index:10}, {char:"p",index:3}, {char:"p",index:2},
{char:"N",index:6}, {char:" ",index:5}, {char:"y",index:4}, {char:"r",index:13}, {char:"H",index:0},
{char:"e",index:11}, {char:"a",index:1}, {char:" ",index:9}, {char:"!",index:14}, {char:"e",index:7}]
```*/







  /*4. Напишите функцию, которая из элементов массива соберет и вернёт
строку, основываясь на index каждой буквы. Например:
```javascript
[{char:"H",index:0}, {char:"i",index: 1}, {char:"!",index:2}] → “Hi!”
```
>Подсказка: вначале отсортируйте массив по index, затем используйте reduce() для построения
строки */








// Массивы. Метод Sort. Задачи.

  /*1. Отсортируйте массив массивов так, чтобы вначале располагались наименьшие массивы (размер массива определяется
   его длиной):
```javascript
[ [14, 45],  [1],  ['a', 'c', 'd']  ] → [ [1], [14, 45], ['a', 'c', 'd'] ]
```*/






  /*2. Есть массив объектов:
```javascript
[
    {cpu: 'intel', info: {cores:2, сache: 3}},
    {cpu: 'intel', info: {cores:4, сache: 4}},
    {cpu: 'amd', info: {cores:1, сache: 1}},
    {cpu: 'intel', info: {cores:3, сache: 2}},
    {cpu: 'amd', info: {cores:4, сache: 2}}
]
```
Отсортировать их по возрастающему количеству ядер (cores).*/







  /*3. Создать функцию, которая будет принимать массив продуктов и две цены. Функция должна вернуть все продукты,
   цена которых находится в указанном диапазоне, и сортировать от дешевых к дорогим:
```javascript

filterCollection(products, 15, 30) → [{...price: 15}, {...price: 18.9}, {...price: 19}, {...price: 25}]
``` */

let products = [
    {title: 'prod1', price: 5.2}, {title: 'prod2', price: 0.18},
    {title: 'prod3', price: 15}, {title: 'prod4', price: 25},
    {title: 'prod5', price: 18.9}, {title: 'prod6', price: 8},
    {title: 'prod7', price: 19}, {title: 'prod8', price: 63}
];

